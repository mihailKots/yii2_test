<?php

namespace app\controllers;

use app\models\Post;
use yii\helpers\ArrayHelper;

class PostController extends AppController
{
  public function actionIndex($name = null)
  {
    $categories = Post::findAll(['category_id'=>'title']);
    $data = Post::find()->select('category_id')->orderBy('id DESC');


    // $posts = Post::find()->select('id, title, excerpt')->all();
    //Get all posts
    $query = Post::find()->select('id, title, excerpt, category_id')->orderBy('id DESC');
    // Get posts with size(2), and deelete GET params from query string(убираем per_page)
    $pages = new \yii\data\Pagination(['totalCount' => $query->count(), 'pageSize' => 4,
    'pageSizeParam' => false, 'forcePageParam' => false]);

    $posts = $query->offset($pages->offset)->limit($pages->limit)->all();
    // $cats = $query->offset($category->offset)->limit($category->limit)->all();
    // $this->debug($posts);
    $littleString = 'hello';
    $helloWorld = 'fellod';
    return $this->render('index', compact('posts', 'pages'));
  }

  public function actionView()
  {
    $id = \Yii::$app->request->get('id');
    $post = Post::findOne($id);
    if(empty($post)){
      throw new \yii\web\HttpException(404, 'No page found srsly');
    }
    return $this->render('view', compact('post'));
  }
}

?>
