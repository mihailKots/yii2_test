-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Окт 09 2018 г., 11:57
-- Версия сервера: 10.1.36-MariaDB
-- Версия PHP: 7.0.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `yii2advanced`
--

-- --------------------------------------------------------

--
-- Структура таблицы `auth_assignment`
--

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('manager', '3', 1538660521),
('superAdmin', '4', 1538660528),
('user', '2', 1538660513);

-- --------------------------------------------------------

--
-- Структура таблицы `auth_item`
--

CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('/admin/*', 2, NULL, NULL, NULL, 1538659931, 1538659931),
('/post/*', 2, NULL, NULL, NULL, 1538659952, 1538659952),
('/rbac/*', 2, NULL, NULL, NULL, 1538659942, 1538659942),
('adminAccess', 2, 'общий доступ в админку', NULL, NULL, 1538660074, 1538660473),
('manager', 1, 'контент-менеджер. Может менять новость или изменять категорию', NULL, NULL, 1538660389, 1538727747),
('postAccess', 2, 'Доступ к чтению постов', NULL, NULL, 1538660017, 1538660017),
('superAdmin', 1, 'главный админ', NULL, NULL, 1538660446, 1538660446),
('updateOwnPost', 2, 'возможность редактировать только свой пост', NULL, NULL, 1538662089, 1539077693),
('updatePost', 2, 'можно изменить пост', NULL, NULL, 1538662036, 1538662036),
('user', 1, 'простой зарегистрированный пользователь', NULL, NULL, 1538660322, 1538660322),
('userAccess', 2, 'доступ к модулю RBAC, для Одменов', NULL, NULL, 1538660234, 1538660234);

-- --------------------------------------------------------

--
-- Структура таблицы `auth_item_child`
--

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('adminAccess', '/admin/*'),
('manager', 'adminAccess'),
('manager', 'updateOwnPost'),
('manager', 'user'),
('postAccess', '/post/*'),
('superAdmin', 'manager'),
('superAdmin', 'userAccess'),
('updateOwnPost', 'updatePost'),
('user', 'postAccess'),
('userAccess', '/rbac/*');

-- --------------------------------------------------------

--
-- Структура таблицы `auth_rule`
--

CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `auth_rule`
--

INSERT INTO `auth_rule` (`name`, `data`, `created_at`, `updated_at`) VALUES
('Author', 0x4f3a32353a226170705c636f6d706f6e656e74735c417574686f7252756c65223a333a7b733a343a226e616d65223b733a363a22417574686f72223b733a393a22637265617465644174223b693a313533383732393537353b733a393a22757064617465644174223b693a313533383732393537353b7d, 1538729575, 1538729575);

-- --------------------------------------------------------

--
-- Структура таблицы `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `parent` int(11) DEFAULT NULL,
  `route` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `data` blob
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1538140228),
('m130524_201442_init', 1538140235),
('m140506_102106_rbac_init', 1538141146),
('m140602_111327_create_menu_table', 1538658910),
('m160312_050000_create_user', 1538658910),
('m170907_052038_rbac_add_index_on_auth_assignment_user_id', 1538141146),
('m180523_151638_rbac_updates_indexes_without_prefix', 1538658971);

-- --------------------------------------------------------

--
-- Структура таблицы `post`
--

CREATE TABLE `post` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `excerpt` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `post`
--

INSERT INTO `post` (`id`, `category_id`, `title`, `excerpt`, `text`, `keywords`, `description`, `created`) VALUES
(1, 1, 'Это первая статья', 'Это первая статья except', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus mi arcu, pulvinar vitae feugiat quis, faucibus quis nulla. Praesent ac ullamcorper est, ac iaculis elit. Morbi posuere ex a dui rhoncus, sit amet porta velit ultrices. In id auctor ante. Curabitur ac sodales tortor. Nam finibus euismod ligula, lacinia dictum justo euismod et. Nulla aliquam velit pretium nibh venenatis, ut efficitur lorem bibendum. Ut pellentesque feugiat nibh et pretium.', 'первая, 1', 'Статья номер 1', '2018-10-02'),
(2, 2, 'Это вторая статья', 'Это статья вторая для тестового', 'Donec mauris quam, tincidunt eget iaculis at, condimentum vel turpis. Mauris pharetra dui vitae maximus gravida. Fusce venenatis tincidunt nisl, accumsan vulputate ex. Praesent porttitor id lorem venenatis malesuada. Pellentesque elementum a neque nec tempor. Quisque nec viverra mauris. Donec dapibus dignissim est at dapibus. Ut quis turpis lobortis, blandit turpis eu, consectetur erat. Cras finibus lacinia magna a rhoncus. Nam id dignissim nulla, eget laoreet nulla.', 'вторая, 2', 'Статья номер 2', '2018-10-02'),
(3, 2, 'Это статья номер 3', 'Статья номер три', 'Integer porttitor est a tempor tempus. Curabitur et sapien accumsan, aliquam massa vel, euismod lacus. Suspendisse ac elementum est. Donec ornare eleifend massa nec posuere. Nunc nec enim sapien. Morbi tincidunt massa turpis, ac interdum lacus ullamcorper ut. Ut posuere ac ligula ut vestibulum. Morbi efficitur accumsan egestas.', 'третья, 3, три', 'Это третья тестовая статья', '2018-10-02'),
(4, 3, 'Это четвертая тестовая статья', '4 тестовая статья', 'Sed efficitur lobortis enim sed vestibulum. Morbi volutpat enim accumsan lacus auctor porttitor et sed purus. Morbi pretium, diam sit amet imperdiet maximus, mauris elit dignissim lorem, interdum scelerisque enim nunc vel enim. In hac habitasse platea dictumst. Duis id neque ac erat pellentesque tristique id vel metus. Aliquam sit amet fringilla nisl. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.', 'четвертая, 4, четыре', 'Четвертая тестовая статья', '2018-10-02'),
(5, 4, 'Этот пост создан из админки', 'Этот пост создан из админки Excerpt', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce condimentum sem vulputate ligula interdum, at auctor odio tincidunt. Curabitur vulputate quam eget arcu scelerisque accumsan vel sit amet mauris. Ut lobortis fermentum risus. Donec fermentum condimentum risus ac congue. Fusce consequat dapibus erat, id blandit nulla iaculis quis. In hac habitasse platea dictumst. Proin ut libero dolor. Cras vel lacus venenatis, facilisis odio eu, aliquam augue. Donec tristique arcu tincidunt aliquet lacinia. Vivamus pellentesque efficitur sem, et vulputate turpis finibus vitae. Donec convallis velit sed sem elementum molestie. Donec ex lectus, blandit eu rutrum in, mollis et eros. Proin libero ante, mollis vel dolor a, ornare bibendum ante. Vivamus et justo at purus suscipit porta. Morbi ultricies est est, et mollis metus semper vitae. Sed massa dui, vulputate vitae leo ut, mollis volutpat eros.\r\n\r\nQuisque iaculis est et odio laoreet, sed placerat sapien venenatis. Nullam vulputate lectus mauris, et egestas orci venenatis sed. Ut ut tellus ipsum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec ac metus sapien. Duis nec quam sodales, tempus justo vitae, pharetra dui. Nulla laoreet dictum tortor et aliquet. Duis id ipsum vel ipsum venenatis mattis. Fusce arcu ex, fermentum vel nulla vitae, dignissim porttitor augue. Maecenas porta quis libero et fringilla. Aliquam ac nisi nec nulla ultricies aliquet. Sed vestibulum auctor nisi, a ullamcorper nulla vulputate eget. Ut erat nulla, auctor eget justo id, vestibulum sagittis justo.\r\n\r\nFusce auctor luctus felis et feugiat. Nam leo ex, mollis at quam eu, ullamcorper euismod tellus. Nunc feugiat ex libero, quis gravida ante sodales in. Etiam fermentum tempus nisl et vulputate. Nulla nec dolor purus. Proin at tortor purus. Curabitur volutpat mauris ante, non egestas nisl viverra sit amet. Suspendisse urna orci, efficitur ac finibus nec, sollicitudin a ipsum. Integer sed mi quis nunc ultrices efficitur eget ut ante. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec sagittis sit amet diam sed lobortis. Curabitur vel pellentesque nibh.\r\n\r\nPraesent consequat odio eu maximus eleifend. Fusce leo ipsum, aliquam ut tortor sit amet, viverra rhoncus ante. Maecenas gravida, elit eu sollicitudin elementum, elit urna laoreet tellus, vel mollis arcu leo vitae libero. Sed ut malesuada nulla. Nulla sapien mauris, semper nec massa rutrum, vestibulum tincidunt lectus. Vestibulum finibus viverra facilisis. Fusce in leo vitae metus hendrerit placerat nec in augue.\r\n\r\nQuisque fermentum consectetur tortor quis interdum. Donec non augue eget metus tempor ultrices. Maecenas diam lacus, mollis vitae elit ac, iaculis pharetra nisl. Cras lobortis eleifend rhoncus. In eget accumsan mi, a vestibulum ex. Donec aliquam nulla nibh. Fusce mattis, enim in viverra placerat, nisi lorem eleifend nisl, at pellentesque massa justo eget neque.', 'админка, создан, изадминки, привет, мир', 'Пост создался сразу после генерирования админской части сайта.', '0000-00-00');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(1, 'rex', 'iGhn-ub8AlDbJ5UH9bq2xF19c3e7SpRf', '$2y$13$iXPveNihZyH.OU3jpK93reBxk0VXaB9lPhf6n.vyqWV4UxBdPJ7li', NULL, 'sk8chaki@gmail.com', 10, 1538142244, 1538142244),
(2, 'user', 'EDrQAVVT23hzOR1G0mjodlVG5kP7VzV4', '$2y$13$Xl3fasAdTU62AoTbIhKMluHALy4VIeSinfVRjkKziMifRXiFUw2fW', NULL, 'examle@example.com', 10, 1538659659, 1538659659),
(3, 'manager', 'KPhfF2Qg2IiBCyR53i4iwSPT-2PKB7aR', '$2y$13$njMZNclH.ZI1TttM4hRp.OurpMJPmhSsoO9sZ83ZPYHdN3fjGQXSy', NULL, 'manager@example.com', 10, 1538659697, 1538659697),
(4, 'superAdmin', '1YUZVrgyjART7RWR0JFGm2-xk6GoqxAA', '$2y$13$qDpmUHYff9eqg/nNamVpeujUV8z/8IsIyZxdpSGfWckrYR.2LOHki', NULL, 'admin@example.com', 10, 1538659766, 1538659766),
(5, 'new', '96sQ8i0PQOwapU3Y1gxt5-QrkAl2GrOz', '$2y$13$u.Yp3FEOkeIHL.x6jHJNZObWpLjofBcagMRJWJij3K2rveZ.k6vJ.', NULL, 'example@maim.com', 10, 1538667317, 1538667317);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`),
  ADD KEY `idx-auth_assignment-user_id` (`user_id`);

--
-- Индексы таблицы `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `idx-auth_item-type` (`type`);

--
-- Индексы таблицы `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Индексы таблицы `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Индексы таблицы `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent` (`parent`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `post`
--
ALTER TABLE `post`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `menu`
--
ALTER TABLE `menu`
  ADD CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `menu` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
