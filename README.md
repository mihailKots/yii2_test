Инструкция для ссылки на сайт без localhost и организация ЧПУ

Положить проект в папку(домен)

Что бы инициализировать проект - php yii init

Выбираем что нужно(dev || prod)

Дамб базы лежит в корне сайта


1. Для создания своих хостов необходимо изменить файл конфигураций
/Applications/XAMPP/xamppfiles/etc/httpd.conf. Открываем его и переходим к строке 173,
в этой строке измените параметр User daemon на User username, где username - имя пользователя в
Вашей системе Mac OS X.
2. Далее включим в работу VirtualHosts. Для этого необходимо раскомментировать строку 488.
3. Теперь нам необходимо сохраним работу http://localhost без изменений.
Для этого откройте файл /Applications/XAMPP/etc/extra/httpd-vhosts.conf и
добавьте в конец этого файла следующий блок:

#localhost
<VirtualHost *:80>
    ServerName localhost
    DocumentRoot "/Applications/XAMPP/xamppfiles/htdocs"
    <Directory "/Applications/XAMPP/xamppfiles/htdocs">
        Options Indexes FollowSymLinks Includes execCGI
        AllowOverride All
        Allow From All
        Order Allow,Deny
    </Directory>
</VirtualHost>

4. Добавляем свой домен, создав папку с проэктом(например - /Users/username/www/test.local)

#test
<VirtualHost *:80>
    ServerName test.local
    DocumentRoot "/Users/username/www/test.local"
    <Directory "/Users/username/www/test.local">
        Options Indexes FollowSymLinks Includes ExecCGI
        AllowOverride All
        Require all granted
    </Directory>
    ErrorLog "logs/test.local-error_log"
</VirtualHost>

5. Добавляем сайт в /etc/hosts
6. Перезапускаем apache

Для того, что бы приложение смотрело в папку web:

7. В корне проэкта создаем .htaccess с след. настройками:

RewriteEngine on
RewriteRule ^(.+)?$ /web/$1

8. В папке web создаем .htaccess с след. настройками:

RewriteBase /
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule . index.php

9. В файле config правим файл web.php, в котором включаем компонент 'urlManager'.
10. Для избавления от папки web - добавляем в компонент 'request' пишем 'baseUrl' => '',
11. Делаем главной страницой контроллер post/index, добвляем настройку 'defaultRoute' => 'post/index' - это будет маршрут
по умолчанию.
12. Далее избавляемся от названия контроллера при переходе между страницами(views), для этого в web.php в компоненте
'urlManager' в 'rules' пишем правило: '<action>' => 'post/<action>', теперь при переходе на на basic/test получаем не 404,
а страницу test, как и с другими страницами.
13. 'urlManager' => [
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'rules' => [
      'post/<id:\d+>' => 'post/view',
      //что бы убрать из адресной строки все кроме post/ и номер страницы
      'page/<page:\d+>' => 'post/index',
      // что бы при обращении к корню, был только корень сайта
      '/' => 'post/index',
    ],
-сначала идут общие правила, потом идут общие.
14. Что бы сгенерировать админку, нужно перейти по пути your_app/gii, там и генерировать сначала модуль, потом модель,
потом CRUD(your_app - доменн сайта).
15. user - user11,
    manager - manager,
    superAdmin - superadmin.
